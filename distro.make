; Use this file to build a full distribution including Drupal core and the
; Commerce Kickstart install profile using the following command:
;
; drush make distro.make <target directory>

api = 2
core = 6.x

projects[drupal][type] = core
projects[drupal][version] = "6"

; Add Commerce Kickstart to the full distribution build.
projects[slim_android_distribution][type] = profile
projects[slim_android_distribution][version] = 1.x-dev
projects[slim_android_distribution][download][type] = git
projects[slim_android_distribution][download][url] = shushu@git.drupal.org:sandbox/shushu/1569304.git

