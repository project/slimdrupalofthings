core = 6.x
api = 2

; Modules
projects[admin_menu][version] = "1.8"
projects[autoload][version] = "2.1"
projects[ctools][version] = "1.8"
projects[cck][version] = "2.9"
projects[features][version] = "1.2"
projects[services][version] = "3.1"
projects[rules][version] = "1.4"
projects[token][version] = "1.18"
projects[views][version] = "2.16"
projects[views_periodic_execution][version] = "1.3"
projects[rules_views][version] = "1.x-dev"
projects[ac2dm][version] = "1.0"
projects[Things][version] = "1.0"

; Profiler
libraries[profiler][download][type] = "get"
libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-6.x-2.0-beta2.tar.gz"

; Sandbox module
projects[node_body_tokens][download][type] = "git"
projects[node_body_tokens][download][url] = "http://git.drupal.org/sandbox/davidstosik/1534924.git"
projects[node_body_tokens][type] = "module"
projects[node_body_tokens][subdir] = "sandbox"

projects[spyc][type] = library
projects[spyc][download][type] = file
projects[spyc][download][url] = http://spyc.googlecode.com/svn/trunk/spyc.php
projects[spyc][download][filename] = "../spyc.php"
projects[spyc][destination] = modules/services/servers/rest_server/lib
